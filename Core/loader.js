var express = require('express'),
	app = express(),
	mode = process.env.NODE_ENV || 'dev',
	bodyParser = require('body-parser'),
	async = require('async'),
	path = require('path'),
	json2xls = require('json2xls'),
	cookieParser = require('cookie-parser'),
	routes = require('../App/routes');
var ClientRoutes = require('../App/handler/client/');

var autoload = require('../App/config/autoload.json');
autoload = autoload[mode] || autoload['dev'];
var fs = require('fs');
var config_database;
var setup = false;
var status_db = false;
var err_db = '';
var target = path.normalize(__dirname+'/..');

var session = require('express-session');
var config_session = require('../App/config/session.json');
config_session = config_session[mode] || config_session['dev'];
var configApp = require('../App/config/app.json')[mode] || require('../App/config/app.json')['dev'];

let multiView = (newPath) => {
	if (newPath) {
		app.set('views', [path.join(target, './App/views'), newPath])
	}
}

let automasiRoute = (object) => {
    let keys = Object.keys(object)
    const router = duku.router

    keys.forEach((k) => {
        let h = object[k]
        k = k.toLowerCase() == 'index' ? '' : k

        router[h.method.toLowerCase()]('/' + k, h.handler)
    })

    return router
}

let duku = {
	router : express.Router(),
	views : multiView,
	request : require('request'),
	componentLayout : target + './App/views/component.jade',
	automasiRoute : automasiRoute
}
// PAYUNI HOST
duku.payuniHost = configApp.app.payuni
global.duku = duku

function Duku() {	
	this.app = app;
	this.db = null;

	this.run = function () {
		configApp.app.host = process.env.OPENSHIFT_NODEJS_IP || configApp.app.host;
		configApp.app.port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || configApp.app.port;
		// CREATE GLOBAL ROUTER
		app.listen(configApp.app.port, configApp.app.host,  function() {
			console.log("Application Running On " + configApp.app.host + ":" + configApp.app.port);
		});
	}

	this.connect = function (cb) {
		if (fs.existsSync('./App/config/database.json')) {
			config_database = require('../App/config/database.json');
			config_database = config_database[mode];
			console.log('iki status setup.e')
			console.log(setup);
			if (config_database) {
				var database = require('./database');
				database(function (err , db){
					this.db = db;
					setup = true;
					if (err) {
						status_db = false;
						err_db = err;
					} else {
						status_db = true;
					}
					return cb(err , db);
				});
			} else {
				setup = false;
				status_db = false;
				return cb('Database Config Not Found', null)
			}
		} else {
			status_db = false;
			return cb('Database Config Not Found', null)
		}
	}

	this.initDatabase = function() {
		if (setup == true) {
			app.use(this.database);
		}
	}

	this.database = function(req, res, next) {
		var driver = require('./driver/' + config_database.driver);
		req['db'] = new driver(this.db);
		next();
	}

	// config session , passport , redis 
	this.initSession = function () {
		if (config_session.redis != undefined ) {
			var	redisStore = require('connect-redis')(session);
			var redis = require('redis').createClient();
			app.use(session({
				secret : config_session.session.secret,
				store : new redisStore({
					host : config_session.redis.host.toString(),
					port : config_session.redis.port,
					saveUninitialized : config_session.session.saveUninitialized,
					resave : config_session.session.resave,
					client : redis,
					db : config_session.redis.db
				}),
				resave : config_session.session.resave
			}));
		} else if (config_session.mongodb != undefined) {
			var MongoStore = require('connect-mongo/es5')(session);
			config_database = require('../App/config/database.json');
			config_database = config_database[mode];
			var url = "";
			var db_session = config_session.mongodb.db;
			if ( (config_session.mongodb.user == '' || config_session.mongodb.user == undefined ) && (config_session.mongodb.password == '' || config_session.mongodb.password == undefined )) {
				url = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://'+config_session.mongodb.host+':' + config_session.mongodb.port + '/'+db_session;
			} else {
				url = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://'+config_session.mongodb.user+':'+config_session.mongodb.password+'@'+config_session.mongodb.host+':' + config_session.mongodb.port + '/'+db_session;
			}
			app.use(session({
				secret: config_session.session.secret,
				// cookie: { maxAge: 24 * 60 * 60  },
				store: new MongoStore({
					url: url
				})
			}));
		} else {
			app.use(session({
				secret : config_session.session.secret,
				saveUninitialized : config_session.session.saveUninitialized,
				resave : config_session.session.resave
			}));
		}

		if (setup == true) {
			if (config_session.passport != undefined ) {
				if (config_session.passport.auth == true ) {
					var authPassport = require('../App/plugins/passport');
					var initPassport = new authPassport();
					var passport = initPassport.getPassport();
					app.use(passport.initialize());
					app.use(passport.session());
					initPassport.passportInit();
				}
			}
		}
	}

	this.initAutoLoad = function() {
		if (setup == true) {
			app.use(this.autoload);
		}
	}

	this.initDuku = function () {
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({ extended: false }));
		// ## SETTING VIEW ENGINE & CSS PRECOMPILER
		var SetupHandler = require('./setup'), Setup = new SetupHandler();
		app.set('views', path.join(target, './App/views'));
		app.set('view engine', 'jade');
		app.use(require('stylus').middleware(path.join(target, './App/public')));
		app.use("/bower", express.static(path.join(__dirname, '../bower_components')));

		app.use(express.static(path.join(target, './App/public')));
		app.use(cookieParser('sessionavocadotetttt'));
		app.use(function (req , res , next ) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "X-Requested-With");
			// res.header("Access-Control-Allow-Credentials", "true");
			next();
		})
		// if (status_db) {
			if (setup == true) {
				if (status_db) {
					app.use(json2xls.middleware);
					var client = express();
					client.set('views', path.join(target, './App/views'));
					client.set('view engine', 'jade');
					client.use(require('stylus').middleware(path.join(target, './App/public')));
					app.use(client);
					ClientRoutes(client);

					routes(app);
				} else {
					app.all('*', function (req , res , next) {
						return res.render('mongodError');
					});
				}
			} else {
				// BUAT SESSION BUATAN
				app.use(session({
					secret : config_session.session.secret,
					saveUninitialized : config_session.session.saveUninitialized,
					resave : config_session.session.resave
				}));

				app.get('/setup', Setup.index);
				app.post('/setup/:step', Setup.setup);
				app.all("*", function (req , res , next) {
					res.redirect("/setup");
				});
			}
		// } else {
		// 	console.log(status_db);
		// }
	}

	this.autoload = function(req , res , next ) {
		var plugins = autoload.plugins || null;
		var features = autoload.features || null;
		req['plugin'] = {};
		if (plugins != null) {
			plugins.forEach(function (plugin) {
				var module = require('../App/plugins/' + plugin);
				req['plugin'][plugin] = new module;
			});
		}

		if (features != null) {
			features.forEach(function (feature) {
				var module = require('../App/features/' + feature);
				req[feature] = new module;
			});
		}
		next();
	}
}

module.exports = Duku;
