function ContentHandler() {
	var async = require('async');
	var striptags = require('striptags');
	var moment = require('moment');
	var _ = require('underscore');

	// CHECK IS PERMALINK ?
	this.permalink = function(req,res,next) {
		var url = req.originalUrl;
		var id = url.split("_")[1];
		req.url = '/item/' + id;
		next();
	}

	this.checkUri = function(req , res , next ) {
		var urlOri = req.originalUrl;
		var id = urlOri.split("_")[1];
		if (id) {
			req.url = '/item/' + id;
		}
		var url = req.url;
		var pecahanUrl = url.substr(1, url.length).split("/");
		var segment1 = '';
		var segment2 = '';
		var segment3 = '';
		var query = {};
		pecahanUrl.forEach(function (v, i) {
			if (i == 0) {
				segment1 = v;
			}
			if (i == 1) {
				segment2 = v;
			}
			if (i == 2) {
				segment3 = v;
			}
		})

		query.uri = "/" + segment1;

		req.db.findOne('uri', query, function (err, hasil) {
			if (err) console.error(err);
			if (hasil) {
				var meta = {};
				meta['url'] = req.originalUrl;
				var titlePage = hasil.title;
				var ModelSite = req.db.collection('site');
				var ModelLayout = req.db.collection('layout');
				var ModelItem = req.db.collection('contentItem');

				// QUERY ITEM
				var queryItem = {};
				queryItem['$and'] = [];

				var widgetData = [];
				var assetsCss = [];
				var assetsJs = [];

				// GABUNGAN QUERY BERSAMA
				async.parallel([
					function (cb) {
						// MENGUERY DATA SEGMENT
						hasil.segment.forEach(function (doc, i) {
							var object = {};
							var regex = pecahanUrl[i+1];
							object['tag.' + doc.label] = {'$in' : [ new RegExp(regex, 'gi')]};

							queryItem['$and'].push(object);
						});
						cb(null, 'Berhasil');
					},
					function (cb) {
						// MENGAMBIL WIDGET DATA
						req.db.find("widgetData", {}, {}, function (err, hasilWidget) {
							widgetData = hasilWidget
							cb(null, "Berhasil");
						})
					},
					function (cb) {
						// MENGAMBIL DATA CSS
						req.db.find("assets", {} , {}, function (err, hasil) {
							hasil.forEach(function (doc) {
								if (doc.type == 'css') {
									assetsCss.push(doc._id + ".min.css");
								}

								if (doc.type == 'js') {
									assetsJs.push(doc._id);
								}
							});
							cb(null, "Berhasil");
						});
					},
					function (cb) {
						req.db.find('contentItem', {}, {}, function(err, hasil) {
							return cb(null, hasil);
						})
					}
				], function (err, hasilGabungan) {
					// MENGAMBIL DATA ITEM DENGAN QUERY YANG SUDAH DI BANGUN UNTUK BOX
					var box = [];
					var dataItem = hasilGabungan[3];
					req.db.find('contentItem', queryItem['$and'].length != 0 ? queryItem : {}, {}, function(err, datanya) {
						// MENGISI BOX YANG ADA DI LAYOUT
						if (err) {
							console.error(new Date(), err);
							next();
						}
						function dynamicSort(property) {
							var sortOrder = 1;
							if(property[0] === "-") {
								sortOrder = -1;
								property = property.substr(1);
							}
							return function (a,b) {
								var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
								return result * sortOrder;
							}
						}

						var prosesBox = function(dataBox, callback) {
							var results = [];
							var i = 0;
							(function next() {
								var doc = dataBox[i++];
								if (!doc) return callback(null, "selesai");
								var dataWidget = req.plugin.customArray.searchObject(doc.widget, widgetData, '_id');
								var tmpDataItem;
								if (dataWidget != undefined) {
									if (segment2 && dataWidget.datauri == true) {
										tmpDataItem = datanya;
									} else {
										tmpDataItem = dataItem;
									}
									// console.log(tmpDataItem);
									if (dataWidget.type == 'populer') {
										req.db.collection('statistik').aggregate([{
											$sort : {
												time : -1
											}
										}, {
											$limit : 1000
										}, {
											$group : {
												_id : {'id_item' : '$id_item'},
												total : {
													$sum : 1
												}
											}
										},{
											$project : {
												_id : 0,
												id : '$_id.id_item',
												total : '$total'
											}
										},{
											$sort : {
												'total' : -1
											}
										}, {
											$limit : 20
										}], function (err, aggregateWidget) {
											// NGUMPUL NO ID
											var id = [];
											aggregateWidget.forEach(function (doc_field) {
												id.push(doc_field.id);
											});
											req.db.find('contentItem', {id : { $in : id }}, {}, function (err, dataPopuler) {
												var dataFix = [];
												dataPopuler.forEach(function (doc_populer) {
													var a = _.findWhere(aggregateWidget, {id : doc_populer.id});
													if (a) {
														doc_populer.total = a.total;
													}
												});

												var filterData = _.filter(dataPopuler, function (item) {
													if (item.id_type == dataWidget.contentType) {
														if ((item.tag[dataWidget.label] != null || dataWidget.label == 'all')) {
															return item;
														}
													}
												});

												tmpDataItem = filterData;

												tmpDataItem.sort(function (a, b) {
													return b.total-a.total;
												});

												// LIMIT
												tmpDataItem = tmpDataItem.slice(0, dataWidget.limit!=""?parseInt(dataWidget.limit):tmpDataItem.length);

												if (dataWidget.view == "carousel") {
													var params = [];
													tmpDataItem.forEach(function (data) {
														if (data.tag[dataWidget.label] || dataWidget.label == 'all') {
															var object = {};
															dataWidget.field.forEach(function(field, i) {
																if (field.type == 'editor') {
																	object[field.value] = striptags(data['editor']['content']);
																}
																else if (field.type == 'images') {
																	object['images'] = data['images'][0];
																}
																else if (field.type == 'time') {
																	object[field.value] = moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss");
																}
																else {
																	object[field.value] = data[field.value];
																}
															})
															params.push(object);
															object = {};
														}
													})
													box.push({
														name : doc.name,
														mixin : dataWidget.view,
														type : 'content',
														params : params
													});
													next();
												} else if (dataWidget.view == "sticky-slider") {
													var params = [];
													tmpDataItem.forEach(function(data) {
														if (data.tag[dataWidget.label] || dataWidget.label == 'all') {
															var object = {};
															if (data.sticky == true) {
																dataWidget.field.forEach(function(field, i) {
																	if (field.type == 'editor') {
																		object[field.value] = striptags(data['editor']['content']);
																	}
																	else if (field.type == 'images') {
																		object['images'] = data['images'][0];
																	}
																	else if (field.type == 'time') {
																		object[field.value] = moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss");
																	}
																	else {
																		object[field.value] = data[field.value];
																	}
																})
																params.push(object);
																object = {};
															}
														}
													})
													box.push({
														name : doc.name,
														mixin : dataWidget.view,
														type : 'content',
														params : params
													});
													next();
												} else {
													tmpDataItem.forEach(function (data) {
														if (data.id_type == dataWidget.contentType) {
															var params = [];
															if ((data.tag[dataWidget.label] || dataWidget.label == 'all') && ((data.tag[dataWidget.label] != undefined && data.tag[dataWidget.label].indexOf(dataWidget.tag) != -1) || dataWidget.tag == 'all' || dataWidget.tag == '')) {
																dataWidget.field.forEach(function(field, i) {
																	// var tambahan = i < doc.field.length-1 ? "," : "";
																	if (field.type == 'editor') {
																		params.push(striptags(data[field.value]['content']));
																	}
																	else if (field.type == 'images') {
																		params.push(data['images']&&data['images'][0]);
																	}
																	else if (field.type == 'time') {
																		params.push(moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss"))
																	}
																	else {
																		params.push(data[field.value]);
																	}
																});
																var objectID = {
																	id : data['id'],
																	id_type : data['id_type'],
																	id_widget : dataWidget._id
																}
																params.push(objectID);
																box.push({
																	name : doc.name,
																	type : 'content',
																	mixin : dataWidget.view,
																	params : params
																});
															}
														}
													});
													next();
												}
											});
										});
									} else if (dataWidget.type == 'data search') {
										if ((segment1 == "item" && dataWidget.view == "item_detail") || (segment1 == "item" && dataWidget.view == "detail-custom")) {
											next();
										}
										else {
											var sortMethod = '';
											if (dataWidget.order.method == "-1") {
												sortMethod = "-";
											}
											else {
												sortMethod = "";
											}
											// QUERY DATA
											var regexType = new RegExp(["^", segment2.split("?")[0], "$"].join(""), "i");
											var whereSearch = {
												type : regexType,
												title : new RegExp(req.query.q, "gi")
											};
											req.db.find('contentItem', whereSearch, {}, function(err, data) {
												// WHERE
												var filterData = _.filter(data, function(item) {
													if (item.id_type == dataWidget.contentType) {
														if ((item.tag[dataWidget.label] != null || dataWidget.label == 'all')) {
															return item;
														}
													}
												})
												// TAMPUNG DATA BARU SETELAH FILTER
												tmpDataItem = filterData;
												// SORT
												tmpDataItem.sort(dynamicSort(sortMethod + dataWidget.order.column));
												// LIMIT
												tmpDataItem = tmpDataItem.slice(0, dataWidget.limit!=""?parseInt(dataWidget.limit):tmpDataItem.length);
												if (segment1 == "search") {
													tmpDataItem.forEach(function (data) {
														if (data.id_type == dataWidget.contentType) {
															var params = [];
															if ((data.tag[dataWidget.label] || dataWidget.label == 'all') && ((data.tag[dataWidget.label] != undefined && data.tag[dataWidget.label].indexOf(dataWidget.tag) != -1) || dataWidget.tag == 'all' || dataWidget.tag == '')) {
																dataWidget.field.forEach(function(field, i) {
																	// var tambahan = i < doc.field.length-1 ? "," : "";
																	if (field.type == 'editor') {
																		params.push(striptags(data[field.value]['content']));
																	}
																	else if (field.type == 'images') {
																		params.push(data['images']&&data['images'][0]);
																	}
																	else if (field.type == 'time') {
																		params.push(moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss"))
																	}
																	else {
																		params.push(data[field.value]);
																	}
																});
																var objectID = {
																	id : data['id'],
																	id_type : data['id_type'],
																	id_widget : dataWidget._id
																}
																params.push(objectID);
																box.push({
																	name : doc.name,
																	type : 'content',
																	mixin : dataWidget.view,
																	params : params
																});
															}
														}
													});
												}
												next();
											})
										}
									} else if (dataWidget.type == 'form search') {
										req.db.findOne('contentType', {id : dataWidget.contentType}, function(err, docContent) {
											if (docContent) {
												box.push({
													name : doc.name,
													mixin : 'item_search',
													type : 'content',
													params : docContent.nama.toLowerCase()
												});
											}
											next();
										})
									} else if (dataWidget.type == 'data list') {
										var sortMethod = '';
										if (dataWidget.order.method == "-1") {
											sortMethod = "-";
										} else {
											sortMethod = "";
										}
										// WHERE
										var filterData = _.filter(tmpDataItem, function(item) {
											if (item.id_type == dataWidget.contentType) {
												if ((item.tag[dataWidget.label] != null || dataWidget.label == 'all')) {
													return item;
												}
											}
										})
										// TAMPUNG DATA BARU SETELAH FILTER
										tmpDataItem = filterData;
										// SORT
										tmpDataItem.sort(dynamicSort(sortMethod + dataWidget.order.column));
										// LIMIT YANG TIDAK STIKY
										if (dataWidget.view != "sticky-slider") {
											tmpDataItem = tmpDataItem.slice(0, dataWidget.limit!=""?parseInt(dataWidget.limit):tmpDataItem.length);
										}
										else {
											tmpDataItem = _.filter(tmpDataItem, function(item) {
												return item.sticky == true;
											})
											tmpDataItem = tmpDataItem.slice(0, dataWidget.limit!=""?parseInt(dataWidget.limit):tmpDataItem.length);
										}
										if ((segment1 == "item" && dataWidget.view == "item_detail") || (segment1 == "item" && dataWidget.view == "detail-custom")) {
											req.db.findOne('contentItem', {id : segment2?segment2:null}, function(err, docItem) {
												// console.log(docItem);
												var url = require('url');
												var urlAnyar = url.parse(req.originalUrl, true);
												var ipClient = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
												var urlRef = req.originalUrl;
												var params = [];
												// console.log(docItem);
												if (docItem) {
													titlePage = docItem.title;
													var ModelStatistik = req.db.collection('statistik');
													req.statistik.setStatistik(req , docItem['id'] , ModelStatistik , {
														ip : ipClient,
														ref : urlRef,
														domain : urlAnyar.query.host,
														user : req.user ? req.user._id : 'guest'
													}, function (err , stast){});
													var most_view = 0;
													if (docItem['most_view'] != undefined) {
														most_view = parseInt(docItem['most_view']) + 1;
													} else {
														most_view += 1;
													}
													req.db.update('contentItem', { id : docItem['id'] }, { $set : { most_view : most_view}}, function (err , rows_update){
														dataWidget.field.forEach(function (field, i) {
															if (field.type == 'editor') {
																params.push(docItem['editor']&&docItem['editor']['content']);
															} else if (field.type == 'images') {
																params.push(docItem['images']&&docItem['images'][0]);
																meta['img'] = docItem['images']&&docItem['images'][0];
																// meta['img'] = docItem[field.value]&&docItem[field.value][0];
															} else if (field.type == 'time') {
																params.push(moment(docItem[field.value]).format("DD MMMM YYYY HH:mm:ss"))
															} else {
																if (field.value == "object") {
																	docItem[field.label] = field.data;
																	field.data.forEach(function(fieldData) {
																		if (fieldData.value.match("Label-")) {
																			var pecahValue = fieldData.value.split("-");
																			var valueLabel = docItem['tag'][pecahValue[1]];
																			fieldData.value = "";
																			if (valueLabel) {
																				valueLabel.forEach(function (d) {
																					var label_text = d&&d.replace(/-/gi,' ');
																					label_text = label_text.trim();
																					var segment_url = d;
																					if (pecahValue[1].toLowerCase() != 'year') {
																						segment_url = segment_url.toLowerCase();
																					}
																					fieldData.value += "<a href=\"/" + pecahValue[1].toLowerCase() + "/" + segment_url + "\" >" + label_text + "</a> ";
																				});
																			}
																		} else {
																			fieldData.value = docItem[fieldData.value];
																		}
																	})
																	params.push(field.data);
																} else {
																	if (dataWidget.view == "detail-custom") {
																		if (field.label == "image") {
																			meta['img'] = docItem[field.value];
																		}
																	}
																	docItem[field.value] = docItem[field.value];
																	params.push(docItem[field.value]);
																}
															}
														});
														// MENCARI TAG
														var objectID = {
															id : docItem['id'],
															title : docItem['title'],
															id_type : docItem['id_type'],
															author : docItem['author'],
															tag : _.keys(docItem['tag']),
															keyword : docItem['tag']
														}
														params.push(objectID);
														box.push({
															name : doc.name,
															mixin : dataWidget.view,
															type : 'content',
															params : params
														});
														// console.log("box 1", box);
														next();
													});
												}
											});
										} else if (dataWidget.view == "sticky-slider") {
											var params = [];
											tmpDataItem.forEach(function(data) {
												if (data.tag[dataWidget.label] || dataWidget.label == 'all') {
													if (data.sticky == true) {
														var object = {};
														dataWidget.field.forEach(function(field, i) {
															if (field.type == 'editor') {
																object[field.value] = striptags(data['editor']['content']);
															}
															else if (field.type == 'images') {
																object['images'] = data['images'][0];
															}
															else if (field.type == 'time') {
																object[field.value] = moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss");
															}
															else {
																object[field.value] = data[field.value];
															}
														})
														object['id'] = data.id;
														params.push(object);
														object = {};
													}
												}
											})
											box.push({
												name : doc.name,
												type : 'content',
												mixin : dataWidget.view,
												params : params
											});
											next();
										}  else if (dataWidget.view == "carousel") {
											var params = [];
											tmpDataItem.forEach(function(data) {
												if (data.tag[dataWidget.label] || dataWidget.label == 'all') {
													var object = {};
													dataWidget.field.forEach(function(field, i) {
														if (field.type == 'editor') {
															object[field.value] = striptags(data['editor']['content']);
														}
														else if (field.type == 'images') {
															object['images'] = data['images'][0];
														}
														else if (field.type == 'time') {
															object[field.value] = moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss");
														}
														else {
															object[field.value] = data[field.value];
														}
													})
													params.push(object);
													object = {};
												}
											})
											box.push({
												name : doc.name,
												mixin : dataWidget.view,
												type : 'content',
												params : params
											});
											next();
										} else {
											tmpDataItem.forEach(function (data) {
												if (data.id_type == dataWidget.contentType) {
													var params = [];
													if ((data.tag[dataWidget.label] || dataWidget.label == 'all') && ((data.tag[dataWidget.label] != undefined && data.tag[dataWidget.label].indexOf(dataWidget.tag) != -1) || dataWidget.tag == 'all' || dataWidget.tag == '')) {
														dataWidget.field.forEach(function(field, i) {
															// var tambahan = i < doc.field.length-1 ? "," : "";
															if (field.type == 'editor') {
																params.push(striptags(data[field.value]['content']));
															}
															else if (field.type == 'images') {
																params.push(data['images']&&data['images'][0]);
															}
															else if (field.type == 'time') {
																params.push(moment(data[field.value]).format("DD MMMM YYYY HH:mm:ss"))
															}
															else {
																params.push(data[field.value]);
															}
														});
														var objectID = {
															id : data['id'],
															id_type : data['id_type'],
															id_widget : dataWidget._id
														}
														params.push(objectID);
														box.push({
															name : doc.name,
															type : 'content',
															mixin : dataWidget.view,
															params : params
														});
													}
												}
											});
											next();
										}
									}
									// IKI SEG TYPENE HTML
									else if (dataWidget.type == 'html') {
										box.push({
											name : doc.name,
											mixin : 'html',
											type : 'content',
											params : dataWidget.code
										});
										next();
									}
									// IKI SEG TYPENE FORM
									else if (dataWidget.type == 'form') {
										req.db.findOne('contentType', {'id' : dataWidget.contentType}, function(err, hasil) {
											if (err) console.error(err);
											if (hasil) {
												box.push({
													name : doc.name,
													type : 'form',
													id_type : hasil.id,
													type_name : hasil.nama,
													component : hasil
												})
												next();
											}
										})
									}
									tmpDataItem = [];
								}
							})();
						}
						// hasil.box.forEach(function (doc) {
						// });
						prosesBox(hasil.box, function(err, data) {
							ModelLayout.findOne({ _id : hasil.layout}, function (err, layout) {
								var layoutnya = layout && layout.layout;
								ModelSite.findOne({}, function (err, doc) {
									if (err) console.error(err);
									if (doc) {
										// MEMASUKKAN CSS DI SITE
										doc.css = assetsCss;
										doc.js = assetsJs;
										req.db.find('navigasi', {} , { sort : { 'created_at' : 1 }}, function (err , Navigasi){
											// MULAI RENDER LAYOUT
											req.attach.use(req, function (hasil) {
												res.render('./client/index', {
													headerCode : hasil.header,
													footerCode : hasil.footer,
													site : doc,
													layout : layoutnya,
													title : titlePage,
													meta : meta,
													// items : dataItem,
													box : box,
													navigasi : Navigasi
												});
											});
										});
									}
								});
							});
						})
					})
				})
			} else {
				// res.send("Gak ono");
				return next();
			}
		})
	}

	// PAGINATION
	this.paging = function(req,res,next) {
		var id_type = req.params.content_type;
		var query = req.query;
		var pathurl = req.query.segment.split("/");
		// LIMIT SEMENTARA PATEN YAH
		var offset = parseInt(query.limit) || 0;
		var batas = parseInt(parseInt(query.page||1)-1) * offset;
		// TOTAL DATA
		var totalData = 0;

		var box = [];

		// SEGMENT
		var segment1 = '';
		var segment2 = '';
		var segment3 = '';
		var queryData = {};
		pathurl.forEach(function (v, i) {
			if (i == 1) {
				segment1 = v;
			}
			if (i == 2) {
				segment2 = v;
			}
			if (i == 3) {
				segment3 = v;
			}
		})

		queryData.uri = "/" + segment1;

		async.parallel({
			uri : function(cb) {
				req.db.findOne('uri', queryData, cb);
			},
			widget : function(cb) {
				req.db.findOne('widgetData', {_id : query.widget||null}, cb)
			}
		}, function (err, hasil) {
			var where = {id_type : id_type};
			var label = hasil.widget&&hasil.widget['label'];

			if (label != 'all') {
				if (segment2) {
					where['tag.' + label] = {$in : [ new RegExp(segment2, 'gi')]};
				} else {
					where['tag.' + label] = {$ne : null};
				}
			}

			// CHECK SEGMENT
			if (hasil['uri']) {
				hasil['uri'].segment.forEach(function(doc, i) {
					where['tag.' + doc.label] = {$in : [ new RegExp(pathurl[parseInt(i) + 2], 'gi')]}
				});
			}

			req.db.find('contentItem', where, {sort : {'time_added' : -1}, skip : batas, limit : offset}, function(err, content) {
				if (content) {
					content.forEach(function(docItem) {
						if (hasil['widget'] != null && docItem != null) {
							var params = [];
							if (docItem.tag[hasil['widget']['label']] || hasil['widget']['label'] == 'all') {
								hasil['widget'].field.forEach(function(field, i) {
									// var tambahan = i < doc.field.length-1 ? "," : "";
									if (field.type == 'editor') {
										params.push(striptags(docItem['editor']['content']));
									} else if (field.type == 'images') {
										params.push(docItem['images']&&docItem['images'][0]);
									} else if (field.type == 'time') {
										params.push(moment(docItem[field.value]).format("DD MMMM YYYY HH:mm:ss"))
									}else {
										params.push(docItem[field.value]);
									}
								})
								var objectID = {
									id : docItem['id'],
									id_type : docItem['id_type'],
									id_widget : hasil['widget']._id
								}
								params.push(objectID);
								box.push({
									// name : doc.name,
									mixin : hasil['widget'].view,
									params : params
								});
							}
						}
					})
				}
				res.json(box);
			});
		})
	}
}
module.exports = ContentHandler;
