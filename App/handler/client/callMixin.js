function CallMixin() {
	this.call = function(req,res,next) {
		var data = req.body.data&&JSON.parse(req.body.data);
		res.render('client/widget/callwidget', {
			mixin : data.mixin,
			params : data.params
		})
	}
}
module.exports = CallMixin;