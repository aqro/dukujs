exports.Index = (req,res,next) => {
	const shortid = require('shortid')
	duku.request.get(duku.payuniHost + '/api/account/list?access_token=' + req.user.access_token, (err, response, body) => {
		// SYNC DATA BANK
		if (response.statusCode == 200) {
			body = JSON.parse(body)
			if (body.status == 'success') {
				const AccountModel = req.db.collection('account_bank')
				body.data.forEach((finfini) => {
					let toInsert = {
						finfini_id : finfini.id,
						name : finfini.nickname,
						bank : finfini.vendor.name,
						rekening : finfini.products,
						author : req.user._id,
						created_at : new Date()
					}
					let nomor_rekenig = []
					finfini.products.forEach((product) => {
						nomor_rekenig.push(product.number)
					})
					toInsert.nomor_rekening = nomor_rekenig.join(",")

					AccountModel.findAndModify(
						{finfini_id : finfini.id},
						[['_id', 'asc']],
						{$set : toInsert},
						{
							new : true,
							upsert : true
						},
						(err, object) => {
							if (err) console.error(err)
							console.log("Sync account")
					})
				})
			}
		}
	})
	res.render('bank')
}

exports.add = (req,res,next) => {
	let elementBank = ''
	duku.request.get(duku.payuniHost + '/api/vendor/list', (err, response, body) => {
		if (response.statusCode == 200) {
			let data = JSON.parse(body).data
			data.forEach((d) => {
				elementBank += "<option value=\"" + d.id + "\">" + d.name + "</option>"
			})

			var obj = [{
				id : 'username',
				label : 'Username',
				type : 'text'
			},{
				id : 'password',
				label : 'Password',
				type : 'password'
			},{
				id : 'vendor_id',
				label : 'Bank',
				type : 'select',
				element : elementBank
			}];
			var tombol = {
				'tambah' : {
					kelas : 'simpan',
					label : 'Simpan',
					url : '/client/bank/addsimpan'
				}
			}
			return res.render('panel/form/dynamic_form',{
				type : 'tambah',
				obj : obj,
				tombol : tombol
			});
		} else {
			console.error(err, response.statusCode, body)
			res.send("Something wrong")
		}
	})
}

exports.dataTable = (req,res,next) => {
	var request = req.query;
	var sColumn = ['_id','bank','nomor_rekening','created_at'];
	var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
	var data = [];
	req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
		if (err) return res.send(kirim);
		HasilAttr.where['author'] = req.user._id
		req.db.find('account_bank', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
			results.forEach(function (i){
				data.push({
					_id : i._id,
					bank : i.bank,
					nomor_rekening : i.nomor_rekening,
					created_at : req.generate.SetTanggal(i.created_at, "DD MMMM YYYY HH:mm:ss")
				});
			});
			req.db.find('account_bank', HasilAttr.where , {} , function (err , list_user){
				req.plugin.datatable.parsingObjectData( sColumn , request , data , list_user.length  , function (err , output ){
					if (err) return res.send(kirim);
					return res.send(JSON.stringify(output));
				});
			});
		})
	});	
}

exports.saveBank = (req,res,next) => {
	req.body.access_token = req.user.access_token
	duku.request.post(duku.payuniHost + "/api/account/create", {
		headers : {
			'content-type' : 'application/json'
		},
		body : JSON.stringify(req.body)
	}, (err, response, body) => {
		if (response.statusCode == 200) {
			res.json({
				status : 'sukses',
				message : body.message,
				data : body
			})
		} else {
			res.json({
				status : 'gagal',
				pesan : JSON.parse(body).data.message.en,
				data : body
			})
		}
	})	
}