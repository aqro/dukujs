let app = duku.router
const {views, componentLayout} = duku
views(__dirname + '/views')
const Bank = require('./controllers/bank')

app.get("/bank", duku.isAuth, Bank.Index)
app.get("/bank/datatable", duku.isAuth, Bank.dataTable)
app.get("/bank/add", duku.isAuth, Bank.add)
app.post("/bank/addsimpan", duku.isAuth, Bank.saveBank)

module.exports = app