function Site () {

	this.index = function (req , res , next ) {
		req.db.findOne('site', {} , function (err , hasil){
			req.db.find('users', {} , { sort : { _id : 1 }}, function (err , User){
				return res.render('panel/site',{
					hasil : hasil,
					user : User
				});
			})
		})
	}

	this.SimpanSite = function (req ,res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data)
		var data = {
			site_name : data.name,
			description : data.desc,
			tagline : data.tagline,
			owner : data.owner,
			admin : data.admin,
			reSiteKey : data.reSiteKey,
			reSecretKey : data.reSecretKey
		}
		req.db.find('site',{} , {} , function (err , rows){
			if (rows.length == 0) {
				data['id'] = 1;
				req.userLog.insert(req, "Menambah Site Baru");				
				req.db.SaveData('site',  data , function (err , doc){
					return res.json('sukses');
				})
			} else {
				req.userLog.insert(req, "Mengubah Site");				
				req.db.update('site', {id : 1}, {$set : data }, function (err , doc){
					return res.json('sukses');
				});
			}
		})
	}

}
module.exports = Site;