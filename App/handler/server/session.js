function SessionHandler () {
	var _ = require('underscore');
	this.index = function (req , res , next ) {
		return res.render('panel/session');
	}

	this.DataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','name','username','email','expires'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('sessions', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					var session = i.session;
					session = JSON.parse(session);
					if (!_.isEmpty(session.passport)) {
						data.push({
							_id : i._id,
							name : session.passport.user.name,
							username : session.passport.user._id,
							email : session.passport.user.email,
							expires : req.generate.SetTanggal(i.expires,'DD MMMM YYYY HH:mm:ss')
						});
					}
				});
				req.db.find('sessions', HasilAttr.where , {} , function (err , list_session){
					arr_sesi = [];
					_.each(list_session , function (sesi){
						var session = sesi.session;
						session = JSON.parse(session);
						if (!_.isEmpty(session.passport)) {
							arr_sesi.push(sesi._id);
						};
					});
					req.plugin.datatable.parsingObjectData( sColumn , request , data , arr_sesi.length  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});
	}
}

module.exports = SessionHandler;