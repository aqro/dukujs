function UriHandler () {

	this.index = function (req , res , next ) {
		return res.render('panel/uri');
	}

	this.DataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['id','uri_name','uri','created_at'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('uri', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				if (err) return res.send(kirim);
				 results.forEach(function (i){
				 	data.push({
				 		id : i.id,
				 		uri_name : i.uri_name,
				 		uri : i.uri,
				 		created_at : req.generate.SetTanggal(i.created_at , 'DD MMMM YYYY HH:mm:ss')
				 	});
				 });
				 req.db.count('uri' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			});
		});
	}

	this.AddUri = function (req , res , next ) {
		req.db.find('layout', {} , { sort : { name : 1 }}, function (err , Layout){
			var ModelLabel = req.db.collection('tag');
			ModelLabel.aggregate([
				{
					$group : {
						_id : '$label'
					}
				},{
					$project : {
						label : '$_id',
						_id : 0
					}
				}
			], function (err , Label){
				req.db.find('contentType', {} , { sort : { nama : 1 }} , function (err , Types){
					var element = req.getArray.CreateElementSelect(Layout , 'tambah' , 'name');
					return res.render('panel/form/uri',{
						type : 'tambah',
						element : element,
						labelTag : Label,
						contentType : Types
					});
				});
			});
		});
	}

	function GetLabel(req , cb) {
		var ModelLabel = req.db.collection('tag');
		ModelLabel.aggregate([
			{
				$group : {
					_id : '$label'
				}
			},{
				$project : {
					label : '$_id'
				}
			}
		], function (err ,results){
			var data = [];
			results.forEach(function (i){
				data.push({
					list : i.label
				});
			});
			return cb(data);
		});
	}

	function GetContentType (req , cb) {
		req.db.find('contentType', {} , {} , function (err , results){
			var data = [];
			results.forEach(function (i){
				data.push({
					list : i.nama
				});
			});
			return cb(data);
		});
	}

	this.GetTagList = function (req , res , next ) {
		var type = req.body.type;
		switch(type){
			case "label" :
				GetLabel(req , function (data){
					return res.json(data);
				});
			break;
			case "contentType" :
				GetContentType(req , function (data){
					return res.json(data);
				});
			break;
		}	
	}

	function CekUri(req , uri , cb ) {
		req.db.find('uri', { uri : uri }, {} , function (err , rows){
			if (rows.length == 0 ) {
				return cb(true);
			} else {
				return cb(false);
			}
		});
	}

	this.SimpanUri = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		var uri = data_json.uri.toLowerCase();
		uri = uri.replace(/[^\w\s]/gi, '');
		data_json.id = req.generate.GetId();
		data_json.created_at = req.generate.Tanggal();
		CekUri(req , uri , function (status){
			if (status == true) {
				req.userLog.insert(req, "Menambah URI Baru Dengan Nama " + b.uri_name);				
				req.db.SaveData('uri', data_json , function (err , rows){
					if (err) return res.json({status : 'gagal'});
					return res.json({status : 'sukses'});
				});
			} else {
				return res.json({status : 'gagal' , pesan : 'name uri duplicate !!!'});
			}
		});
	}

	this.GetFieldType = function (req , res , next ) {
		var type = req.body.type;
		req.db.find('contentType', { id : type }, {} , function (err , rows){
			var data = [{ type : 'string', value : 'title'}, {type : 'time', value : 'time_added'}, {
					type : 'string', value : 'id'}, {type : 'string' , value : 'author'},{type : 'string' , value : 'id_type'},{ type : 'string', value : 'most_view'}];
			if (rows.length != 0 ) {
				rows[0].itemObject.forEach(function (i){
					if (data.indexOf(i.type) == -1) {
						// data.push(i.type);
						if (i.type == 'label_tag') {
							data.push({
								type : i.type,
								value : "Label-" + i.label
							});
						}
						else {
							data.push({
								type : i.type,
								value : i.label
							});
						}
					}
				});
			}
			// console.log(data);
			return res.json(data);
		})
	}

	this.GetUri = function (req , res , next ) {
		var id = req.params.id;
		req.db.find('uri', { id : id }, {} , function (err , rows){
			if (rows.length == 0) { return next() };
			req.db.find('layout', {} , { sort : { name : 1 }}, function (err , Layout){
				req.db.find('contentType', {} , { sort : { nama : 1 }} , function (err , Types){
					var element = req.getArray.CreateElementSelect(Layout , 'edit' , 'name' , rows[0].layout);
					var ModelLabel = req.db.collection('tag');
					ModelLabel.aggregate([
						{
							$group : {
								_id : '$label'
							}
						},{
							$project : {
								label : '$_id',
								_id : 0
							}
						}
					], function (err , Label){
						req.db.find('widgetData', {}, {}, function(err, hasilWidget) {
							return res.render('panel/form/uri',{
								type : 'edit',
								element : element,
								contentType : Types,
								labelTag : Label,
								data  : rows[0],
								widget : hasilWidget
							});
						})
					});
				});
			});
		});
	}

	this.UpdateUri = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		var uri = data_json.uri.toLowerCase();
		uri = uri.replace(/[^\w\s]/gi, '');
		var id = b.id;
		req.db.find('uri',{ uri : uri }, {} , function (err , row){
			if (row.length == 1 && id == row[0].id ) {
				req.userLog.insert(req, "Mengubah URI Dengan Nama " + row.uri_name);		
				req.db.update('uri' , { id : b.id } , { $set : data_json }, function (err , results){
					if (err) return res.json({status : 'gagal'});
					return res.json({status : 'sukses'});
				});
			} else if (row.length == 0 ) {
				req.db.update('uri' , { id : b.id } , { $set : data_json }, function (err , results){
					if (err) return res.json({status : 'gagal'});
					return res.json({status : 'sukses'});
				});
			} else {
				return res.json({status : 'gagal' , pesan : 'name uri duplicate !!!'});
			}
		});
	}

	this.DeleteUri = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		var allowedID = [];
		req.userLog.insert(req, "Menghapus URI Dengan ID " + data.id.join(", "));
		req.db.find('uri', {id : {$in : data.id }}, function(err, rows) {
			rows.forEach(function(r) {
				if (r.deleteable != false) {
					allowedID.push(r.id);
				}
			})
			req.db.remove('uri',{ id : { $in : data.id }}, function (err , rows ){
				return res.json({status : 'sukses'});
			});
		})
	}

	this.GetLayout = function (req , res , next ) {
		var id_layout = req.body.id_layout;
		req.db.find('layout', { _id : id_layout }, {} , function (err , rows){
			if (rows.length != 0 ) {
				var Layout = rows[0].layout
				req.getArray.getNameBox(Layout , function (box_name){
					return res.json(box_name);
				});
			} else {
				return res.json([]);
			}
		});
	}
	
	this.getBox = function(req,res,next) {
		var layout = req.body.id;
		req.db.findOne('layout', {_id : layout}, function(err, hasil) {
			if (err) console.error(err);
			if (hasil) {
				res.json(hasil);
			}
		});
	}
}
module.exports = UriHandler;