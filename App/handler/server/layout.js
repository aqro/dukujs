function LayoutHandler() {
	var tpl = "panel/layoutGenerator";
	var shortid = require('shortid');

	this.index = function(req,res,next) {
		res.render(tpl + '/index')
	}

	this.save = function(req,res,next) {
		var layout = JSON.parse(req.body.layout);
		var data = JSON.parse(req.body.data);

		var user = null;
		if (req.user) {
			user = req.user._id;
		}
		var dataToInsert = {
			_id : shortid.generate(),
			name : data.name,
			time_created : req.generate.Tanggal(),
			time_updated : '',
			author : user,
			history : [],
			layout : layout
		}
		// req.userLog.insert(req, "Membuat Layout Baru Dengan Nama " + data.name);		
		req.db.SaveData('layout', dataToInsert, function(err, hasil) {
			if (err) return res.json({status : 500, pesan : err})
			res.json({status : 200, pesan : 'Layout Has been saved'})
		})
	}

	this.editSave = function(req,res,next) {
		var id = req.params.id;
		var layout = JSON.parse(req.body.layout);
		var data = JSON.parse(req.body.data);

		var user = null;
		if (req.user) {
			user = req.user._id;
		}
		var dataToUpdate = {
			name : data.name,
			time_updated : req.generate.Tanggal(),
			author : user,
			// history : [],
			layout : layout
		}
		// req.userLog.insert(req, "Mengedit Layout Dengan Nama " + data.name);		
		req.db.update('layout', {_id : id}, {$set : dataToUpdate}, function(err, hasil) {
			if (err) return res.json({status : 500, pesan : err})
			res.json({status : 200, pesan : 'Layout Has been saved'})
		})		
	}

	this.dataTable = function(req,res,next) {
		var request = req.query;
		var sColumn = ['_id','name','author','time_created','time_updated'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('layout', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						author : i.author,
						time_created : req.generate.SetTanggal(i.time_created , 'DD MMMM YYYY HH:mm:ss'),
						time_updated : req.generate.SetTanggal(i.time_updated , 'DD MMMM YYYY HH:mm:ss')
					});
				});

				req.db.count('layout' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});	
	}

	this.add = function(req,res,next) {
		var ref = req.headers['x-requested-with'];
		req.db.find('contentType', {}, {}, function(err, hasil) {
			if (err) console.error(err);
			if (ref == 'XMLHttpRequest') {
				return res.render(tpl + '/add', {
					contentType : hasil
				});
			}
			else {
				return res.redirect('/main#' + req.url)
			}
		})
	}

	this.edit = function(req,res,next) {
		var id = req.params.id;

		req.db.findOne('layout', {_id : id}, function(err, doc) {
			var ref = req.headers['x-requested-with'];
			if (ref == 'XMLHttpRequest') {
				req.db.find('contentType', {}, {}, function(err, hasil) {
					if (err) console.error(err);
					return res.render(tpl + '/add', {
						layout : doc.layout,
						type : 'edit',
						data : doc,
						contentType : hasil
					});
				});
			}
			else {
				return res.redirect('/main#' + req.url)
			}
		})
	}

	this.hapus = function(req,res,next) {
		var id = JSON.parse(req.body.data).id;
		// req.userLog.insert(req, "Menghapus Layout Dengan ID " + id.join(", "));
		req.db.remove('layout', {_id : {$in : id}}, function(err, hasil) {
			if (err) console.error(err);
			res.json({status : 'success', msg : 'Delete Success'})
		})
	}
}
module.exports = LayoutHandler;