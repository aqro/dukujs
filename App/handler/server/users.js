function UserHandler () {

	this.index = function (req , res , next ) {
		return res.render('panel/user');
	}

	this.FromLogin = function (req , res , next ) {
		return res.render('panel/login',{
			title : 'Login'
		})
	}

	this.ProccessLogin = function (req , res , next ) {
		req.plugin.passport.Login(req , res , next );
	}

	this.DataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['id','name','_id','nomor','created_at','updated_at'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('users', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						id : i.id,
						name : i.name,
						_id : i._id,
						nomor : i.nomor,
						created_at : req.generate.SetTanggal(i.created_at , 'DD MMMM YYYY HH:mm:ss'),
						updated_at : req.generate.SetTanggal(i.updated_at , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('users' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});
	}

	this.Logout = function (req , res , next ) {
		var redirect = '/';
		if (req.header('referer') != undefined )
			redirect = req.header('referer');
		req.logout();
		res.redirect(redirect);
	}

	this.TambahBaru = function (req , res , next ) {
		var obj = [{
			id : 'name',
			label : 'Name',
			type : 'text'
		},{
			id : 'username',
			label : 'Username',
			type : 'text'
		},{
			id : 'password',
			label : 'Password',
			type : 'password'
		},{
			id : 'email',
			label : 'Email',
			type : 'text'
		},{
			id : 'nomor',
			label : 'Nomor Hp',
			type : 'text'
		}];
		var tombol = {
			'tambah' : {
				kelas : 'simpan',
				label : 'Simpan',
				url : '/manage/user/simpan'
			}
		}
		return res.render('panel/form/dynamic_form',{
			type : 'tambah',
			obj : obj,
			tombol : tombol
		});
	}

	this.SimpanUser = function (req , res , next ) {
		var b = req.body;

		// FETCH PAYUNI USER
        duku.request.post(duku.payuniHost + "/api/user/create", {
            headers : {
                'content-type' : 'application/json'
            },
            body : JSON.stringify(b)
        }, (err, response, body) => {
            if (response.statusCode == 200) {
                res.json({
                    status : 'sukses',
                    message : body.message,
                    data : body
                })
            } else {
                res.json({
                    status : 'gagal',
                    pesan : JSON.parse(body).data.message.en,
                    data : body
                })
            }
        })
	}

	this.GetDataUser = function (req , res , next ) {
		var id = req.params.id;
		req.db.find('users', { id : id }, {} , function (err , user){
			if (err) next(err);
			req.db.find('s_group', {}, {}, function (err , Group){
				var UserLogin = req.user;
				var cek = UserLogin.role_group.indexOf('root');
				var status = '';
				if (cek == -1) {
					status = 'user';
				} else {
					status = 'root';
				}
				return res.render('panel/form/editor_user',{
					data : user[0],
					group : Group,
					status : status
				});
			});
		})
	}

	this.PushGroup = function (req , res , next ) {
		var b = req.body;
		var id_user = b.id_arg;
		var id_group = b.id_list;
		req.db.update('users', { _id : id_user }, { $push : { role_group : id_group }}, function (err ,  rows){
			if (err) return res.json({status : 'gagal', pesan : 'Tambah Group Gagal !!!'});
			return res.json({status : 'sukses'})
		});
	}

	this.PullGroup = function (req , res , next ) {
		var b = req.body;
		var id_user = b.id_arg;
		var id_group = b.id_list;
		req.db.update('users', { _id : id_user }, { $pull : { role_group : id_group }}, function (err ,  rows){
			if (err) return res.json({status : 'gagal', pesan : 'Hapus Group Gagal !!!'});
			return res.json({status : 'sukses'})
		});
	}

	this.EditUser = function (req , res , next ) {
		var b = req.body;
		var name = b.name;
		var email = b.email;
		var nomor = b.nomor;
		var data = {
			name : name,
			email : email,
			nomor : nomor,
			updated_at : req.generate.Tanggal()
		}
		if (req.validasi.ValidEmail(email) == true ) {
			req.userLog.insert(req, "Mengedit User " + data.id);			
			req.db.update('users', { _id : b.id } ,{ $set : data } , function (err , rows){
				if (err) return res.json({status : 'gagal' , pesan : 'Edit User Gagal !!!'});
				return res.json({status : 'sukses'})
			});
		} else {
			return res.json({status : 'gagal' , pesan : 'Email Failed !!!'});
		}
	}

	this.Delete = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		req.userLog.insert(req, "Menghapus User Dengan ID " + data.id);		
		req.db.remove('users', {id : { $in : data.id }}, function (err , rows){
			return res.json({status : 'sukses'})
		})
	}

	this.GantiPassword1 = function (req , res , next ) {
		var b = req.body;
		try {
			req.db.findOne('users',{ _id : b.id }, function (err , user){
				var status = req.generate.CheckBcrypt(user.password , b.lama);
				if (status == 'ya') {
					var password = req.generate.password(b.baru)
					req.db.update('users',{_id : b.id } , { $set : { password : password }}, function (err ,rows){
						return res.json({status : 'sukses'});
					})
				} else {
					return res.json({status : 'gagal' , pesan : 'Password Tidak Sama !!!'});
				}
			});
		} catch (e) {
			return res.josn({status : 'gagal' , pesan : e});
		}
	}

	this.GantiPassword2 = function (req , res , next ) {
		var b = req.body;
		try {
			var password = req.generate.password(b.baru);
			req.userLog.insert(req, "Mengganti Password " + b.id);
			req.db.update('users',{_id : b.id } , { $set : { password : password }}, function (err ,rows){
				return res.json({status : 'sukses'});
			});
		}catch (e){
			return res.josn({status : 'gagal' , pesan : e});
		}
	}
}

module.exports = UserHandler;