const crypto = require('crypto')
exports.Index = {
    method : "GET",
    handler : (req,res,next) => {
        res.render('user-payuni')
    }
}

exports.Add = {
    method : "GET",
    handler : (req,res,next) => {
		var obj = [{
			id : 'name',
			label : 'Name',
			type : 'text'
		},{
			id : 'username',
			label : 'Username',
			type : 'text'
		},{
			id : 'password',
			label : 'Password',
			type : 'password'
		},{
			id : 'email',
			label : 'Email',
			type : 'text'
		}];
		var tombol = {
			'tambah' : {
				kelas : 'simpan',
				label : 'Simpan',
				url : '/payuni-users/addsimpan'
			}
		}
		return res.render('panel/form/dynamic_form',{
			type : 'tambah',
			obj : obj,
			tombol : tombol
		});        
    }
}

exports.addsimpan = {
    method : "POST",
    handler : (req,res,next) => {
        let users = req.body
        duku.request.post(duku.payuniHost + "/api/user/create", {
            headers : {
                'content-type' : 'application/json'
            },
            body : JSON.stringify(req.body)
        }, (err, response, body) => {
            if (response.statusCode == 200) {
                res.json({
                    status : 'sukses',
                    message : body.message,
                    data : body
                })
            } else {
                res.json({
                    status : 'gagal',
                    pesan : JSON.parse(body).data.message.en,
                    data : body
                })
            }
        })
    }
}

exports.dataTable = {
    method : "GET",
    handler : (req,res,next) => {
		var request = req.query;
		var sColumn = ['_id','name','username','password','email','client_key','created_at'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('payuni_users', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
                    data.push({
                        _id : i._id,
                        name : i.name,
                        username : i.username,
                        password : i.password,
                        email : i.email,
                        client_key : i.client_key,
                        created_at : req.generate.SetTanggal(i.created_at, "DD MMMM YYYY HH:mm:ss")
                    });
				});
				req.db.find('payuni_users', HasilAttr.where , {} , function (err , list_user){
					req.plugin.datatable.parsingObjectData( sColumn , request , data , list_user.length  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});        
    }
}