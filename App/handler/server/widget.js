function WidgetHandler() {

	this.list = function(req,res,next) {
		if (!req.query.id) {
			req.db.find('widgetView', {}, {}, function(err, hasil) {
				if (err) {
					console.error(err);
					next();
				} else {
					return res.json(hasil);
				}
			})
		} else {
			req.db.findOne('widgetView', {_id : escape(req.query.id)}, function(err, hasil) {
				if (err) {
					console.error(err);
					next();
				} else {
					return res.json({status : 200, data : hasil});
				}
			})
		}
	}

	this.dataList = function(req,res,next) {
		if (!req.query.id) {
			req.db.find('widgetData', {}, {}, function(err, hasil) {
				if (err) {
					console.error(err);
					next();
				} else {
					return res.json(hasil);
				}
			})
		} else {
			req.db.findOne('widgetData', {_id : escape(req.query.id)}, function(err, hasil) {
				if (err) {
					console.error(err);
					next();
				} else {
					return res.json({status : 200, data : hasil});
				}
			})
		}
	}

	function GetContentType (req , cb) {
		req.db.find('contentType', {} , {} , function (err , results){
			var data = [];
			results.forEach(function (i){
				data.push({
					list : i.nama,
					id : i.id
				});
			});
			return cb(data);
		});
	}

	function getFieldContentType(req, data, cb) {
		//////////////////////////////////// -> IKI SEG KUNO KUDU NE VALUE NE SEG DI TAMPIL NO ORA TYPE NE <- //////////////////////////
		// req.db.find('contentType', { id : data }, {} , function (err , rows){
		// 	var data = ['title', 'time_added', 'id', 'author','id_type'];
		// 	if (rows.length != 0 ) {
		// 		rows[0].itemObject.forEach(function (i){
		// 			if (data.indexOf(i.type) == -1 && i.type != 'label_tag') {
		// 				data.push(i.type);
		// 			}
		// 		});
		// 	}
		// 	return cb(data);
		// })		

		req.db.find('contentType', { id : data }, {} , function (err , rows){
			var data = [{ type : 'string', value : 'title'}, {type : 'time', value : 'time_added'}, {
					type : 'string', value : 'id'}, {type : 'string' , value : 'author'},{type : 'string' , value : 'id_type'},{ type : 'string', value : 'most_view'}];
			if (rows.length != 0 ) {
				rows[0].itemObject.forEach(function (i){
					if (data.indexOf(i.type) == -1) {
						// data.push(i.type);
						if (i.type == 'label_tag') {
							data.push({
								type : i.type,
								value : "Label-" + i.label
							});
						}
						else {
							data.push({
								type : i.type,
								value : i.label
							});
						}
					}
				});
			}
			// console.log(data);
			return cb(data);
		})
	}
	
	this.index = function (req , res , next ) {
		return res.render('panel/widgetData');
	}

	// dataTable widget data layout
	this.DataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','name','type','created_at'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('widgetData', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						type : i.type,
						created_at : req.generate.SetTanggal(i.created_at , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('widgetData' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});
	}

	// tambah widget data layout
	this.TambahWidget = function (req , res , next ) {
		req.db.find('widgetView', {}, {}, function(err, hasil) {
			GetContentType(req, function (contentType) {
				var ModelLabel = req.db.collection('tag');
				ModelLabel.aggregate([
					{
						$group : {
							_id : '$label'
						}
					},{
						$project : {
							label : '$_id',
							_id : 0
						}
					}
				], function (err, hasilLabel) {
					return res.render('panel/form/widgetData',{
						type : 'tambah',
						viewWidget : hasil,
						contentType : contentType,
						field : "",
						label : hasilLabel
					});
				});
			});
		});
	}

	// simpan widget data layout
	this.SimpanWidget = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		data_json['_id'] = req.generate.GetId();
		data_json['created_at'] = req.generate.Tanggal();
		req.userLog.insert(req, "Menambah Widget Baru Dengan Nama " + data_json.name);
		req.db.SaveData('widgetData', data_json , function (err , rows){
			if (err) return res.json({status : 500, pesan : 'simpan widget gagal !!!'});
			return res.json({status : 200});
		});
	}

	// menampilkan widget data layout
	this.GetDataWidget = function (req , res , next ) {
		var id = req.params.id;
		req.db.findOne('widgetData',{_id : id } , function (err , rows){
			if (err || rows == null ) return next();
			req.db.find('widgetView', {}, {}, function (err, hasil) {
				GetContentType(req, function(contentType) {
					getFieldContentType(req, rows.contentType, function (field) {
						var ModelLabel = req.db.collection('tag');
						ModelLabel.aggregate([
							{
								$group : {
									_id : '$label'
								}
							},{
								$project : {
									label : '$_id',
									_id : 0
								}
							}
						], function(err, hasilLabel) {
							return res.render('panel/form/widgetData',{
								type : 'edit',
								data : rows,
								contentType : contentType,
								viewWidget : hasil,
								field : JSON.stringify(field),
								label : hasilLabel
							});
						});
					})
				});
			});
		})
	}

	// edit widget data layout
	this.EditWidget = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		var id = b.id;
		req.userLog.insert(req, "Mengedit Widget Data Dengan Nama " + data_json.name);
		req.db.update('widgetData', { _id : id } , { $set : data_json } , function (err , rows){
			if (err) return res.json({status : 'gagal', pesan : 'edit widget gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

	this.GetFieldType = function (req , res , next ) {
		var type = req.body.type;
		req.db.find('contentType', { id : type }, {} , function (err , rows){
			var data = ['title', 'time_added', 'id', 'author'];
			if (rows.length != 0 ) {
				rows[0].itemObject.forEach(function (i){
					data.push(i.type);
				});
				return res.json(data);
			} else {
				return res.json(data);
			}
		})
	}

	this.DeleteWidgetData = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		req.userLog.insert(req, "Delete Widget Data Dengan ID " + data.id);		
		req.db.remove('widgetData', { _id : { $in : data.id }}, function (err , rows){
			return res.json({status : 'sukses'});
		})
	}

	// WIDGET VIEW

	this.indexView = function (req , res , next ) {
		return res.render('panel/widgetView');
	}

	// dataTable widget view
	this.DataTableView = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','name','mixin','field','time_added'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('widgetView', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						mixin : i.mixin,
						field : i.field,
						time_added : req.generate.SetTanggal(i.time_added , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('widgetView' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});
	}

		// tambah widget view
	this.TambahWidgetView = function (req , res , next ) {
		return res.render('panel/form/widgetView',{
			type : 'tambah'
		});
	}

	// simpan widget view
	this.SimpanWidgetView = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		data_json['_id'] = req.generate.GetId();
		data_json['time_added'] = req.generate.Tanggal();
		req.db.SaveData('widgetView', data_json , function (err , rows){
			if (err) return res.json({status : 'gagal', pesan : 'simpan widget gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

		// menampilkan data widget view
	this.GetDataWidgetView = function (req , res , next ) {
		var id = req.params.id;
		req.db.findOne('widgetView',{_id : id } , function (err , rows){
			if (err || rows == null ) return next();
			return res.render('panel/form/widgetView',{
				type : 'edit',
				data : rows
			});
		});
	}
		// edit widget view
	this.EditWidgetView = function (req , res , next ) {
		var b = req.body;
		var data_json = JSON.parse(b.data);
		var id = b.id;
		req.db.update('widgetView', { _id : id } , { $set : data_json } , function (err , rows){
			if (err) return res.json({status : 'gagal', pesan : 'edit widget gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

	this.EditWidgetView = function (req , res , next ) {
		var b = req.body;
		var id = b.id;
		var data_json = JSON.parse(b.data);
		req.db.update('widgetView', { _id : escape(id)} , { $set : data_json }, function (err , rows){
			if (err) return res.json({status : 'gagal', pesan : 'edit widget gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

		// delete widget view
	this.DeleteWidgetView = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		req.db.remove('widgetView', {_id : { $in : data.id }} , function (err , rows){
			return res.json({status : 'sukses'});
		})
	}

	// GET TAG IN LABEL
	this.getTag = function(req,res,next) {
		var label = req.body.label;
		req.db.find('tag', {label : label}, {}, function(err, hasil) {
			if (err) console.error(err);
			return res.json(hasil);
		})
	}
}
module.exports = WidgetHandler;