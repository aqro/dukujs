function AttachHandler() {
	this.index = function(req,res,next) {
		return res.render('panel/attach');
	}
	this.dataTable = function(req,res,next) {
		var request = req.query;
		var sColumn = ['_id','name','location','author','created_at','time_updated'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('attach', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						location : i.location,
						author : i.author,
						created_at : req.generate.SetTanggal(i.created_at , 'DD MMMM YYYY HH:mm:ss'),
						time_updated : req.generate.SetTanggal(i.time_updated , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('attach' , HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});	
	}
	this.tambah = function(req,res,next) {
		var elementSelect = "";
		var dataSelect = ['header', 'footer'];
		dataSelect.forEach(function(doc) {
			elementSelect += "<option value=\"" + doc + "\">" + doc + "</option>";
		})
		var obj = [{
			id : 'name',
			label : 'Name',
			type : 'text'
		},{
			id : 'code',
			label : 'Code',
			type : 'area'
		},{
			id : 'location',
			label : 'Location',
			type : 'select',
			element : elementSelect
		}];
		var tombol = {
			'tambah' : {
				kelas : 'simpan',
				label : 'Simpan',
				url : '/manage/attach/simpanTambah'
			}
		}
		return res.render('panel/form/dynamic_form',{
			type : 'tambah',
			obj : obj,
			tombol : tombol
		});
	}
	this.edit = function(req,res,next) {
		var id = req.params.id;
		req.db.findOne('attach', {_id : id}, function(err, data) {
			if (err) {
				console.error(err);
				next();
			}
			else {
				if (data) {
					var elementSelect = "";
					var dataSelect = ['header', 'footer'];
					dataSelect.forEach(function(doc) {
						var selected = data.location == doc && "selected=" + true;
						elementSelect += "<option value=\"" + doc + "\"" + selected + ">" + doc + "</option>";
					})
					var obj = [{
						id : 'name',
						label : 'Name',
						type : 'text',
						value : data&&data.name
					},{
						id : 'code',
						label : 'Code',
						type : 'area',
						value : data&&data.code
					},{
						id : 'location',
						label : 'Location',
						type : 'select',
						element : elementSelect
					}];
					var tombol = {
						'edit' : {
							kelas : 'simpan-edit',
							label : 'Simpan',
							url : '/manage/attach/simpanEdit',
							id : id
						}
					}
					return res.render('panel/form/dynamic_form',{
						type : 'edit',
						obj : obj,
						tombol : tombol
					});
				}
				else {
					next();
				}
			}
		})
	}

	this.simpanTambah = function(req,res,next) {
		var data = req.body;
		data._id = req.generate.GetId();
		data.created_at = req.generate.Tanggal();
		data.author = req.user._id || 'Anonymouse';

		req.userLog.insert(req, "Menambah Attach Baru Dengan Nama " + data.name);
		req.db.SaveData('attach', data, function(err, hasil) {
			if (err) {
				console.error(err);
				res.json({'status' : 'gagal', pesan : err});
			}
			else {
				res.json({'status' : 'sukses', pesan : 'Attach Berhasil Di Tambah'});
			}
		})
	}

	this.simpanEdit = function(req,res,next) {
		var data = req.body;
		data.time_updated = req.generate.Tanggal();
		req.db.findOne('attach', {_id : data.id}, function(err, doc) {
			if (doc) {
				req.userLog.insert(req, "Edit Attach Dengan Nama " + doc.name);				
				req.db.update('attach', {_id : data.id}, {$set : data}, function(err, hasil) {
					if (err) {
						console.error(err);
						res.json({'status' : 'gagal', pesan : err})
					}
					else {
						res.json({'status' : 'sukses', pesan : "Berhasil Edit Attach"});
					}
				})				
			}
			else {
				next();
			}
		})
	}

	this.delete = function(req,res,next) {
		var id = JSON.parse(req.body.data).id;
		req.userLog.insert(req, "Menghapus Attach Dengan ID " + id.join(", "));
		req.db.remove('attach', {_id : {$in : id}}, function(err, hasil) {
			if (err) {
				console.error(err);
				res.json({'status' : 'gagal', pesan : err});
			}
			else {
				res.json({'status' : 'sukses', pesan : "Berhasil Hapus Attach"})
			}
		})
	}
}
module.exports = AttachHandler;