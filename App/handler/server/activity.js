function ActivityHandler() {
	this.index = function(req,res,next) {
		return res.render('panel/activity');
	}
	this.dataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','user','description','time'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('userLog', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						user : i.user,
						description : i.description,
						time : req.generate.SetTanggal(i.time , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('userLog' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});
	}
}
module.exports = ActivityHandler;