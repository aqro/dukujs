function NavigasiHandler () {

	this.index = function (req , res , next ) {
		return res.render('panel/navigasi');
	}

	this.DataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','label','url','created_at'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			req.db.find('navigasi', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						label : i.label,
						url : i.url,
						created_at : req.generate.SetTanggal(i.created_at , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('navigasi' ,HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			});
		});
	}

	this.TambahNavigasi = function (req , res , next ) {
		return res.render('panel/form/navigasi',{
			type : 'tambah'
		});
	}

	this.SimpanNavigasi = function (req , res , next ) {
		var b = req.body;
		var data = {
			_id : req.generate.GetId(),
			label : b.label,
			url :b.url,
			created_at : req.generate.Tanggal()
		}
		req.userLog.insert(req, "Menambah Menu Navigasi Baru Dengan Judul " + data.label);		
		req.db.SaveData('navigasi', data , function (err , rows){
			if (err) return res.json({status : 'gagal' , pesan : 'Simpan Navigasi Gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

	this.GetDataNavigasi = function (req , res , next ) {
		var id = req.params.id;
		req.db.findOne('navigasi', { _id : id }, function (err , rows){
			if (err || rows == null ) return next();
			return res.render('panel/form/navigasi',{
				type : 'edit',
				data : rows
			});
		});
	}

	this.EditNavigasi = function (req , res , next ) {
		var b = req.body;
		var data = {
			label : b.label,
			url : b.url
		}
		req.userLog.insert(req, "Edit Menu Navigasi Dengan Judul " + data.label);		
		req.db.update('navigasi', { _id : b.id } , { $set : data }, function (err , rows){
			if (err) return res.json({status : 'gagal' , pesan : 'Edit Navigasi Gagal !!!'});
			return res.json({status : 'sukses'});
		});
	}

	this.DeleteNavigasi = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		req.userLog.insert(req, "Menghapus Navigasi Dengan ID " + data.id);		
		req.db.remove('navigasi',{ _id : { $in : data.id }} , function (err , rows){
			return res.json({status : 'sukses'});
		});
	}
}

module.exports = NavigasiHandler;