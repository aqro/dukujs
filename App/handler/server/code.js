function CodeHandler() {
	var fs = require('fs');
	var path = require('path');
	var async = require('async');
	var cleanCSS = require('clean-css');

	// HANDLER CSS EDITOR
	this.cssIndex = function(req,res,next) {
		return res.render('panel/code');
	}

	this.cssTambah = function(req,res,next) {
		return res.render('panel/form/code-editor', {
			type : 'tambah'
		});
		// return res.send('test');
	}

	this.cssDataTable = function(req,res,next) {
		var request = req.query;
		var sColumn = ['_id','name','author','time_created','time_updated'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			HasilAttr.where['type'] = 'css';
			req.db.find('assets', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						author : i.author,
						time_created : req.generate.SetTanggal(i.time_created , 'DD MMMM YYYY HH:mm:ss'),
						time_updated : req.generate.SetTanggal(i.time_updated , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('assets' , HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});	
	}

	this.cssEdit = function(req,res,next) {
		var id = req.params.id;
		req.db.findOne('assets', { type : 'css', _id : id}, function(err, hasil) {
			if (err) {
				console.error(err);
				next();
			} else {
				if (hasil) {
					res.render('panel/form/code-editor', {
						type : 'edit',
						data : hasil,
						encodeData : JSON.stringify(hasil)
					});
				} else {
					next();
				}
			}
		})
	}

	this.cssSimpan = function(req,res,next) {
		var data = req.body;
		data['_id'] = req.generate.GetId();
		data['type'] = 'css';
		data['author'] = req.user&&req.user._id;
		data['time_created'] = req.generate.Tanggal();
		var pathFile = path.join(__dirname, "../../public/dist/css/");
		req.db.findOne('assets', {type : 'css', name : data.name }, function (err, doc) {
			if (err) {
				console.error(err);
				next();
			} else {
				if (!doc) {
					req.userLog.insert(req, "Tambah Css Dengan Nama " + data.name);
					fs.writeFile(pathFile+data._id + ".css" , data.code , function (err){
						if (err) {
							console.error(err);
							next();
						} else {
							req.db.SaveData('assets', data, function (err, hasil) {
								if (err) {
									console.error(err);
									next();
								} else {
									var minifiedCss = new cleanCSS().minify(data.code).styles;
									fs.writeFile(pathFile+ data._id + '.min.css' , minifiedCss , function (err){
										if (err) {
											console.error(err);
											next();
										} else {
											res.json({status : 200});
										}
									});
								}
							});
						}
					});
				} else {
					res.json({status : 403, err : 'File Sudah Ada'});
				}
			}
		})
	}

	this.cssEditSimpan = function(req,res,next) {
		var body = req.body;
		var data = JSON.parse(body.data);
		data['time_updated'] = req.generate.Tanggal();
		var pathFile = path.join(__dirname, "../../public/dist/css/");
		// MENULIS FILE CSS
		fs.writeFile(pathFile + body.id + ".css", data.code, function (err) {
			if (err) {
				console.error(err);
				next();
			} else {
				req.db.update('assets', {type : 'css', _id : body.id}, {$set : data}, function(err, hasil) {
					if (err) {
						console.error(err);
						next();
					} else {
						req.userLog.insert(req, "Edit Css Dengan Nama " + data.name);						
						var minifiedCss = new cleanCSS().minify(data.code).styles;
						fs.writeFile(pathFile + body.id + '.min.css', minifiedCss, function (err) {
							if (err) {
								console.error(err);
								next();
							} else {
								res.json({status : 200, pesan : 'Data Has Been Updated'});
							}
						})
					}
				})				
			}
		})
	}

	this.CssDelete = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		var target = path.join(__dirname, "../../public/dist/css/");
		async.waterfall([
			function (callback) {
				var no = 1;
				req.db.find('assets', { _id : { $in : data.id } } , { } , function (err , hasil){
					req.userLog.insert(req, "Delete Css Dengan ID " + data.id);
					hasil.forEach(function (i){
						fs.unlink(target+i.name, function (err){
							req.db.remove('assets',{ type : 'css' , _id : i._id }, function (err , rows){
								if (no == hasil.length) {
									return callback(null , null);
								}
								no = no + 1;
							});
						});
					});
				});
			}
		], function (err , results){
			console.log(results);
			return res.json({status : 'sukses'});
		});
	}

	// HANDLER JS EDITOR
	this.jsIndex = function (req , res , next ) {
		return res.render('panel/code_js');
	}

	this.jsDataTable = function (req , res , next ) {
		var request = req.query;
		var sColumn = ['_id','name','author','time_created','time_updated'];
		var kirim = JSON.stringify({"iTotalDisplayRecords":0,"iTotalRecords":0,"aaData":[]});
		var data = [];
		req.plugin.datatable.olahAttr(sColumn , request , function (err , HasilAttr){
			if (err) return res.send(kirim);
			HasilAttr.where['type'] = 'js';
			req.db.find('assets', HasilAttr.where , { sort : HasilAttr.order , limit : HasilAttr.limit , skip : HasilAttr.offset }, function (err , results){
				results.forEach(function (i){
					data.push({
						_id : i._id,
						name : i.name,
						author : i.author,
						time_created : req.generate.SetTanggal(i.time_created , 'DD MMMM YYYY HH:mm:ss'),
						time_updated : req.generate.SetTanggal(i.time_updated , 'DD MMMM YYYY HH:mm:ss')
					});
				});
				req.db.count('assets' , HasilAttr.where , function (err , jumlah){
					if (err) return res.send(kirim);
					req.plugin.datatable.parsingObjectData( sColumn , request ,data , jumlah  , function (err , output ){
						if (err) return res.send(kirim);
						return res.send(JSON.stringify(output));
					});
				});
			})
		});	
	}

	this.jsTambah = function (req , res , next ) {
		return res.render('panel/form/js-editor',{
			type : 'tambah'
		});
	}

	this.jsSimpan = function (req , res , next ) {
		var b = req.body;
		// var data = JSON.parse(b.data);
		var target = path.join(__dirname, "../../public/dist/js/");
		b['_id'] = req.generate.GetId();
		b['type'] = 'js';
		b['author'] = req.user&&req.user._id;
		b['time_created'] = req.generate.Tanggal();
		b['name'] = b['name'].substr(0, b['name'].length-3);

		req.db.findOne('assets', {type : 'js', name : b.name }, function (err, rows) {
			if (err) {
				console.error(err);
				next();
			} else {
				fs.writeFile(target+b._id+".js" , b.code , function (err){
					req.userLog.insert(req, "Tambah Javascript Dengan Nama " + b.name);
					if (err) return next(err);
					// var minifiedCss = new cleanCSS().minify(b.code).styles;
					// fs.writeFile(target+b.name.substr(0, b.name.length-3)+'.min.js', minifiedCss , function (err){
					// 	if (err) return next(err);
					req.db.SaveData('assets',b , function (err , rows){
						if (err) return res.json({status : 'gagal', pesan : 'file gagal di simpan !!!'});
						return res.json({status : 'sukses'});
					})
					// })
				});
			}
		});
	}

	this.jsGetData = function (req , res , next ) {
		var id = req.params.id;
		req.db.findOne('assets', {type : 'js' , _id : id }, function (err , rows){
			if (err) {
				console.error(err);
				return next();
			} else {
				return res.render('panel/form/js-editor',{
					type : 'edit',
					data : rows,
					encodeData : JSON.stringify(rows)
				});
			}
		})
	}

	this.jsEditData = function (req , res , next ) {
		var body = req.body;
		var data = JSON.parse(body.data);
		data['time_updated'] = req.generate.Tanggal();
		data['name'] = data['name'].substr(0, data['name'].length-3);
		var target = path.join(__dirname, "../../public/dist/js/");
		fs.writeFile(target + body.id+".js", data.code, function (err) {
			if (err) {
				console.error(err);
				return res.json({status : 'gagal' , pesan : 'kode gagal di simpan !!!'})
			} else {
				req.userLog.insert(req, "Edit Javascript Dengan Nama " + data.name);
				// var minifiedCss = new cleanCSS().minify(data.code).styles;
				// fs.writeFile(target+data.name.substr(0, data.name.length-3)+'.min.js', minifiedCss , function (err){
				// 	if (err) return res.json({status : 'gagal' , pesan : 'kode gagal di simpan !!!'})
				req.db.update('assets',{ _id : body.id , type : 'js' }, { $set : data } , function (err , rows){
					if (err) return res.json({status : 'gagal', pesan : 'file gagal di edit !!!'});
					return res.json({status : 'sukses'});
				});
				// })
			}
		});
	}

	this.jsDelete = function (req , res , next ) {
		var b = req.body;
		var data = JSON.parse(b.data);
		var target = path.join(__dirname, "../../public/dist/js/");
		async.waterfall([
			function (callback) {
				var no = 1;
				req.userLog.insert(req, "Delete Javascript Dengan ID " + data.id);				
				req.db.find('assets', { _id : { $in : data.id } } , { } , function (err , hasil){
					hasil.forEach(function (i){
						fs.unlink(target+i.name, function (err){
							req.db.remove('assets',{ type : 'js' , _id : i._id }, function (err , rows){
								if (no == hasil.length) {
									return callback(null , null);
								}
								no = no + 1;
							});
						});
					});
				});
			}
		], function (err , results){
			console.log(results);
			return res.json({status : 'sukses'});
		});
	}

}
module.exports = CodeHandler;