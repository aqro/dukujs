function Maps() {
	this.nearby = function(req,res,next) {
		var geospatial = [];
		var q = req.query.q;
		// console.log(req.headers);
		geospatial = geospatial.concat(parseFloat(req.query.longitude || 0), parseFloat(req.query.latitude || 0));
		// console.log(req.query);
		var where = {
			location : {
				$near : {
					$geometry : {
						type : "Point",
						coordinates : geospatial
					},
					$maxDistance : parseFloat(req.query.max || 0),
					$minDistance : parseFloat(req.query.min || 0)
				}
			},
			$or : [{
				title : new RegExp(q, "gi")
			}, {
				description : new RegExp(q, "gi"),
			}]
		}

		console.log(where);
		req.db.find('lokasi', where, {}, function(err, data) {
			var hasilMurni = [];
			if (err) throw err;
			console.log(data);
			data.forEach(function(doc) {
				doc.latitude = doc.location && doc.location.coordinates[1];
				doc.longitude = doc.location && doc.location.coordinates[0];
				doc.type = doc.location && doc.location.type;

				delete doc.location;
				hasilMurni.push(doc);
			})
			res.json(data);
		})		
	}

	this.addLocation = function(req,res,next) {
		var geospatial = [];
		var data = req.body;
		// console.log(data);
		geospatial = geospatial.concat(parseFloat(data.longitude || 0), parseFloat(data.latitude || 0));
		if (data.title == "") {
			return res.status(401).json({status : 401, msg : "Title Can't be empty"});
		}
		var dataToInsert = {
			_id : req.generate.GetId(),
			title : data.title,
			description : data.description,
			location : {
				type : "Point",
				coordinates : geospatial
			},
			created_at : req.generate.Tanggal()
		}
		req.db.SaveData('lokasi', dataToInsert, function(err, hasil) {
			if (err) return res.status(500).json({status : 500, msg : err});
			return res.json({status : 200, msg : "Success"});
		})
	}
}
module.exports = Maps;