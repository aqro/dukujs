function Content() {
	var _ = require('underscore');

	this.find = function(req,res,next) {
		var contentType = req.params['name'];
		var label = req.query['label'];
		var tag = req.query['tag'];
		var nearby = req.query['nearby'];
		var coordinate = req.query['location']
		var max = req.query['distance']
		var where = {
			type : contentType
		}
		if (nearby) {
			var loc = coordinate.split(",")
			var lat = loc[0]
			var long = loc[1]
			var geospatial = [];
			geospatial = geospatial.concat(parseFloat(long || 0), parseFloat(lat || 0));

			where[nearby] = {
				$near : {
					$geometry : {
						type : "Point",
						coordinates : geospatial
					},
					$maxDistance : parseFloat(max || 0),
					$minDistance : parseFloat(req.query.min || 0)
				}
			}
		}
		if (label) {
			var dataLabel = [];
			if (label.match("-")) {
				where['$or'] = [];
				dataLabel = label.split("-");
			}
			else if (label.match("_")) {
				where['$and'] = [];
				dataLabel = label.split("_");
			}
			else {
				dataLabel.push(label);
			}
			dataLabel.forEach(function(d) {
				var object = {};
				object['tag.' + d] = {
					$exists : true
				}
				if (label.match("_")) {
					where['$and'].push(object);
					where['$and'].push(searchTag);
				}
				else if (label.match("-")) {
					where['$or'].push(object);
					where['$or'].push(searchTag);
				}
				else {
					where['tag.' + d] = {
						$exists : true
					}
				}
			})
			if (label && tag) {
				var tagLabel = [];
				tag.split("|").forEach(function(l) {
					tagLabel.push(l);
				})
				dataLabel.forEach(function(d) {
					where['tag.' + d] = {
						$in : [new RegExp(tagLabel, "gi")]
					}
				});
			}
		}

		var limit = parseInt(req.query.limit) || 100;
		var skip = parseInt(req.query.skip) || 0;
		var sort = req.query.sort || 'time_added';
		var methodSort = isNaN(req.query.by) ? -1 : parseInt(req.query.by);
		req.db.find('contentItem', where, {'sort' : {sort : methodSort}, limit : limit, skip : skip}, function(err, data) {
			if (err) return res.status(500).json(err);
			res.json(data);
		});
	}
	this.insert = function(req,res,next) {
		var body = req.body;
		req.db.findOne('contentType', {nama : req.params['name']}, function(err, data) {
			if (err) throw err;
			if (data) {
				data.itemObject.push({
					label : "title",
					type : "string"
				});
				var column = data.itemObject;
				if (Array.isArray(body)) {
					var dataToInsert = [];
					body.forEach(function(doc) {
						var object = {
							id : req.generate.GetId(),
							id_type : data.id,
							type : data.nama,
							status : 'aktif',
							author : req.dataUser&&req.dataUser._id
						};
						object['tag'] = {};
						column.forEach(function(d) {
							if (doc[d.label]) {
								if (d.type == "label_tag") {
									doc[d.label] = [].concat(doc[d.label]);
									object['tag'][d.label] = doc[d.label];
								}
								else if (d.type == "images") {
									var objectImage = doc[d.label];
									var arrayImg = [];
									if (Array.isArray(doc[d.label])) {
										objectImage.forEach(function(img) {
											var imgObj = {
												imageUrl : img.url,
												poster : img.poster?img.poster:false
											};
											arrayImg.push(imgObj);
										})
									}
									else if (typeof(objectImage) === "object") {
										var imgObj = {
											imageUrl : objectImage.url,
											poster : objectImage.poster?objectImage.poster:false
										};
										arrayImg.push(imgObj);
									}
									object['images'] = arrayImg;
								}
								else {
									object[d.label] = doc[d.label];
								}
							}
							else {
								if (d.type == "label_tag") {
									doc[d.label] = [];
									object['tag'][d.label] = doc[d.label];
								}
								else {
									object[d.label] = "";
								}								
							}
						})
						object.time_added = req.generate.Tanggal()
						dataToInsert.push(object);
					});
					if (dataToInsert.length != 0) {
						req.db.SaveData('contentItem', dataToInsert, function(err, result) {
							// SEND EMAIL
							if (data.notif) {
								req.db.findOne('site', {}, function (err, hasil) {
									if (hasil) {
										var toEmail = [];
										req.db.find('users', {_id : {$in : hasil.owner}}, {}, function (err, doc) {
											doc.forEach(function(dataUser) {
												toEmail.push(dataUser.email);
											})
											dataToInsert.forEach(function(data_json) {
												res.render('client/mailer', {
													nama : "API Programming",
													pesan : "You Have New Item From " + req.params.name,
													desc : "<b>" + data_json.title + "</b> now been added in your content type <b>" + data_json.type + "</b>"
												}, function (err, html) {
													var mail = {
														from : 'support@dukujs.com',
														to : toEmail,
														subject : '#' + data_json.title + " From " + req.params.name,
														html : html
													}
													req.mailer.send(mail, req, function (err, response) {
														console.log("Mengirim Notifikasi");
													})
												})
											})
										})
									} else {
										console.error("Ora ono site e ");
									}
								})
							}

							res.json({
								status : 200,
								msg : "Successfull insert item " + req.params.name
							});
						})			
					}
					else {
						res.status(400).json({
							status : 200,
							msg : "Nothing Data To Insert"
						})
					}
				}
				else {
					res.status(403).send("Body must be an array");
				}
			}
			else {
				res.status(404).send("Content Type Not Found");
			}
		});
	}

	this.search = function(req,res,next) {
		var query = req.query.q.split(" ");
		// console.log(query);
		var queryMurni = [];
		query.forEach(function(d) {
			d = new RegExp(d, "gi");
			queryMurni.push(d)
		})
		var where = {
			_id : {
				$in : queryMurni
			}
		}
		
		req.db.find('tag', where, {}, function(err, tag) {
			if (err) throw err;
			var array_label = [];
			var queryTag = [];
			tag.forEach(function(i) {
				if (array_label.indexOf(i.label) == -1) {
					var tag = {};
					tag['tag.' + i['label']] = i._id;
					queryTag.push(tag);
					array_label.push(i.label);
				}
			})
			// console.log(queryTag);
			// queryTag = queryTag.concat({
			// 	title : req.query.q
			// })
			var queryItem = {
				$or : [{
					title : {
						$in : queryMurni.concat(new RegExp(req.query.q, "gi"))
					},
					// 'tag.Keyword' : {
					// 	$in : queryMurni.concat(new RegExp(req.query.q, "gi"))
					// }
				}]
			};
			if (queryTag.length != 0) {
				queryItem['$or'] = [];
				queryItem['$or'].push({
					$and : queryTag
				})
			}

			console.log(queryItem['$or'][0]);
			// res.json(tag);
			req.db.find('contentItem', queryItem, {}, function(err, item) {
				// console.log(err);
				res.json({error : err, data : item});
			})
		})
	}
}
module.exports = Content;