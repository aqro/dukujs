function Tag() {
	this.get = function(req,res,next) {
		req.db.find('tag', {}, {}, function(err, data) {
			res.json(data);
		})
	}

	this.perLabel = function(req,res,next) {
		req.db.find('tag', {label : new RegExp(req.params['label'], "gi")}, {}, function(err, data) {
			res.json(data);
		})
	}

	this.label = function(req,res,next) {
		var ModelTag = req.db.collection('tag');
		ModelTag.aggregate([
			{
				$group : {
					_id : {
						label : '$label'
					}
				}
			}
		], function(err, data) {
			var hasil = [];
			data.forEach(function(doc) {
				hasil.push(doc._id.label)
			})
			res.json(hasil);
		})
	}
}
module.exports = Tag;