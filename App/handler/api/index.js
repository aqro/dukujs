module.exports = exports = function(ex) {
	var express = require('express');
	var app = express();
	app.set('view engine', 'jade')
	app.set('views', __dirname + '/../../views/')
	var UserHandler = require('./users'), User = new UserHandler();
	var ContentHandler = require('./content'), Content = new ContentHandler();
	var TagHandler = require('./tag'), Tag = new TagHandler();
	var MapHandler = require('./map'), Map = new MapHandler();

	app.get("/", function(req,res,next) {
		res.send("Welcome in API Duku");
	})

	// USER
	app.get("/user/oauth/token", User.login)
	app.get("/user/info", User.checkToken, User.info);
	app.get("/user/check/:nomor", User.accountKit);
	app.post("/user/register", User.register);

	// CONTENT
	app.get("/content/get/:name", User.checkToken, Content.find);
	app.post("/content/insert/:name", User.checkToken, Content.insert);
	app.get("/content/search", User.checkToken, Content.search);
	app.get("/content/nearby/:name", User.checkToken, Map.nearby);

	// TAG
	app.get("/get/label", User.checkToken, Tag.label);
	app.get("/get/tag", User.checkToken, Tag.get);
	app.get("/get/tag/:label", User.checkToken, Tag.perLabel);

	ex.use("/api", app);
}