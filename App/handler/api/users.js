function User() {
	var bcrypt = require('bcrypt-nodejs');
	var moment = require('moment');
	this.checkToken = function(req,res,next) {
		var tokenMentah = req.query.token || req.headers.token || req.body.token;
		if (tokenMentah) {
			req.token.decrypt(req, tokenMentah, function(token) {
				if (token) {
					req.dataUser = token;
					next();
				}
				else {
					res.status(401).send("Access Token Not Valid");
				}
			})
		}
		else {
			res.status(401).send("You need access token");
		}
	}

	this.login = function(req,res,next) {
		var username = req.query.username;
		var password = req.query.password;

		req.db.findOne('users', {_id : username}, function(err, data) {
			if (err) throw err;
			if (data) {
				if (bcrypt.compareSync( password , data.password ) ) {
					var token = req.generate.GetId() + "#" + moment().format("YYYY-MM-DD HH:mm:ss") + "#" + username + "#" + moment().unix();
					req.token.encrypt(req, token, username, function(dataToken) {
						var dataUser = {
							username : data._id,
							token : dataToken,
							expired : 3600
						}
						res.json(dataUser);
					})
				}
				else {
					res.status(401).send("Password did'nt match")
				}
			}
			else {
				res.statusCode = 404;
				res.send("User Not Found");
			}
		})
	}

	this.info = function(req,res,next) {
		res.json(req.dataUser);
	}

	this.accountKit = function(req,res,next) {
		req.db.findOne('users', {nomor : req.params.nomor}, (err, data) => {
			if (err) return next(err)
			if (data) {
				var token = req.generate.GetId() + "#" + moment('9999-12-31 23:59:59').format("YYYY-MM-DD HH:mm:ss") + "#" + data._id + "#" + moment().unix();
				req.token.encrypt(req, token, data._id, function(dataToken) {
					var dataUser = {
						username : data._id,
						token : dataToken
					}
					res.json(dataUser);
				})
			} else {
				res.json({
					status : 404,
					message : 'Not Found'
				})
			}
		})
	}

	this.register = function(req,res,next) {
		var b = req.body;
		console.log(req.body)
		var username = b.username;
		var name = b.name;
		var nomor = b.nomor;
		var role = b.role;
		b.password = 'newuser';
		var password = req.generate.password(b.password);
		var created_at = req.generate.Tanggal();
		var data = {
			id : req.generate.GetId(),
			name : name,
			_id : username,
			password : password,
			nomor : nomor,
			status : 1,
			created_at : created_at,
			role_group : [b.role]
		}
		req.userLog.insert(req, "Menambah User " + data._id);
		req.db.SaveData('users', data , function (err , rows){
			if (err) return res.json({status : 403 , pesan : 'Simpan User Gagal'});
			return res.json({status : 200, message : 'OK'})
		});
	}
}
module.exports = User;