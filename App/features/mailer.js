var mode = process.env.NODE_ENV || 'dev';
var confMailler = require('../config/mailler.json');
var configMailer = confMailler[mode] || confMailler['dev'];

function Mailer() {
	var nodemailer = require('nodemailer');
	// var mg = require('nodemailer-mailgun-transport');
	var smtpTransport = require('nodemailer-smtp-transport');

	var smtpConfig = {
		service : "gmail",
		auth: {
			user: configMailer.auth.user,
			pass: configMailer.auth.pass
		}
	};

	var transport = nodemailer.createTransport(smtpConfig);

	// var auth = {
	// 	auth : {
	// 		api_key : 'key-f29ab9dca144b345551e782252784742',
	// 		domain : 'steamhdmovies.com'
	// 	}
	// }

	this.send = function(options, req, cb) {
		//////// VERSI MAILGUN /////////////
		// var mode = process.env.NODE_ENV || 'dev';
		// if (mode != 'dev') {
			transport.sendMail(options, function (err, info) {
				var dataMailer = {
					_id : req.generate.GetId(),
					from : options.from,
					to : options.to,
					subject : options.subject,
					time : req.generate.Tanggal(),
					body : options.html
				}
				req.db.SaveData('mailer', dataMailer, function (err, hasil) {
					if(err) {
						// throw err;
						console.error(err);
					} else {
						console.log("Insert To Mailer");
					}
					return cb(err, info);
				})
			})
		// }
		// else {
		// 	return cb("err", null);
		// }
	}
}
module.exports = Mailer;