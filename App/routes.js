module.exports = exports = function (app) {
	// Handler Server
	var GenerateHandler = require('./handler/server/generate'), Generate = new GenerateHandler();
	var UserHandler = require('./handler/server/users') , User = new UserHandler();
	var GroupHandler = require('./handler/server/group') , Group = new GroupHandler();
	var MenuHandler = require('./handler/server/menu') , Menu = new MenuHandler();
	var SessionHandler = require('./handler/server/session') , SessionUser = new SessionHandler();
	var ContentHandler = require('./handler/server/content'), Content = new ContentHandler();
	var SiteHandler = require('./handler/server/site'), Site = new SiteHandler();

	var authPassport = require('../App/plugins/passport'), Session = new authPassport();
	global.duku.isAuth = Session.isAuthenticated

	var ErrorHandler = require('./handler/error').errorHandler;
	var ErrorHandlerLogs = require('./handler/server/dataError'), LogsError = new ErrorHandlerLogs();
	var BackupRestoreHandler = require('./handler/server/backupRestore'), Backup = new BackupRestoreHandler();

	var shortid = require('shortid');
	var upload = require('jquery-file-upload-middleware')
	var ErrorHandlerLogs = require('./handler/server/dataError'), LogsError = new ErrorHandlerLogs();
	var shortid = require('shortid');

	app.use('/upload/image',  upload.fileHandler({
		uploadDir: __dirname + '/public/media/image',
		uploadUrl: '/file/upload/image'
	}));

	upload.on('begin', function (fileInfo , req , res ) {
		// console.log('iki request body');
		fileInfo.name = shortid.generate()+fileInfo.name;
		fileInfo.originalName =  shortid.generate()+fileInfo.name;
	});

	// ======================================== HANDLER SERVER ======================================
	app.get('/main', Session.isAuthenticated , Content.index);
	app.get('/dashboard', Session.ProtectMenu ,Content.Dashboard);
											// ROUTE HALAMAN LOGIN
	app.get('/user/login', User.FromLogin)
	app.post('/user/login', User.ProccessLogin);
	app.get('/user/logout', User.Logout);
											// ROUTE USER MANAGEMENT
	app.get('/manage/user', Session.ProtectMenu , User.index);
	app.get('/manage/user/dataTable', Session.isAuthenticated , User.DataTable);
	app.get('/manage/user/add', Session.isAuthenticated , User.TambahBaru);
	app.post('/manage/user/simpan', Session.isAuthenticated , User.SimpanUser);
	app.get('/manage/user/edit/:id', Session.isAuthenticated , User.GetDataUser);
	app.post('/manage/user/pushGroup', Session.isAuthenticated , User.PushGroup);
	app.post('/manage/user/edit', Session.isAuthenticated , User.EditUser);
	app.post('/manage/user/pullGroup', Session.isAuthenticated , User.PullGroup);
	app.post('/manage/user/delete', Session.isAuthenticated , User.Delete);
	app.post('/manage/user/gatiPassword', Session.isAuthenticated , User.GantiPassword1);
	app.post('/manage/user/gatiPassword2', Session.isAuthenticated , User.GantiPassword2);

											// ROUTE GROUP MANAGEMENT
	app.get('/manage/group', Session.ProtectMenu , Group.index);
	app.get('/manage/group/dataTable', Session.isAuthenticated , Group.DataTable);
	app.get('/manage/group/add', Session.isAuthenticated , Group.GroupBaru);
	app.post('/manage/group/simpan' , Session.isAuthenticated , Group.SimpanGroup);
	app.post('/manage/group/delete', Session.isAuthenticated , Group.DeleteGroup);

											// ROUTE MENU MANAGEMENT
	app.get('/manage/menu', Session.ProtectMenu , Menu.index);
	app.post('/manage/menu/delete', Session.isAuthenticated , Menu.DeleteMenu);
	app.get('/manage/menu/edit/:id', Session.isAuthenticated , Menu.GetMenu);
	app.post('/manage/menu/edit', Session.isAuthenticated , Menu.EditMenu);
	app.get('/manage/menu/add' , Session.isAuthenticated , Menu.TambahMenu);
	app.post('/manage/menu/simpan' , Session.isAuthenticated , Menu.SimpanMenu);
	app.post('/manage/menu/getAccess' , Session.isAuthenticated , Menu.GetAccess);
	app.post('/manage/menu/SetAccess', Session.isAuthenticated , Menu.SetAccess);

											// ROUTE SITE MANAGEMENT
	app.get('/manage/site', Session.ProtectMenu , Site.index );
	app.post('/manage/site/simpan', Session.isAuthenticated , Site.SimpanSite);

	//////////////////////////////////////// MANAGE ERROR HANDLE /////////////////////////////

	app.get("/manage/logs/error", Session.ProtectMenu , LogsError.index);
	app.get("/manage/logs/error/dataTable", Session.isAuthenticated , LogsError.dataTable);

	app.get("/manage/backup-restore", Session.ProtectMenu , Backup.index);
	app.get("/manage/backup-restore/tambah", Session.isAuthenticated, Backup.tambah);
	app.post("/manage/backup-restore/delete", Session.isAuthenticated, Backup.Delete);
	app.post("/manage/backup-restore/backup", Session.isAuthenticated, Backup.backup);
	app.post("/manage/backup-restore/restore", Session.isAuthenticated, Backup.restore);
	app.get("/manage/backup-restore/dataTable", Session.isAuthenticated, Backup.dataTable);
	app.post('/manage/backup', Session.isAuthenticated , Backup.BackupData);

	app.get("/", (req,res,next) => {
		res.send("Hello world from payuni")
	})
	// app.get('/:permalink', ContentClient.checkUri);
	// app.get('/:permalink', ContentClient.permalink, ContentClient.checkUri);

	// LOAD NEW HANDLER
	app.use(require('./handler/server/account'))
	app.use("/client", require('./handler/client/payuni'))
	app.use(Content.notFound);
	app.use(ErrorHandler);
}
